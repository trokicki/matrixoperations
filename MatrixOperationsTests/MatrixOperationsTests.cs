﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using MatrixOperations;

namespace MatrixOperationsTests {
	[TestFixture]
	public class MatrixOperationsTests {
		[Test]
		public void MatrixDeterminantTest() {
			double[,] matrix = {
				{-1, -4},
				{-2, -6}
			};
			Assert.That(MatrixOperation.Determinant(matrix), Is.EqualTo(-2));
			matrix = new double[,]{
				{1, 2, 3},
				{0, 1, 4},
				{5, 6, 0}
			};
			Assert.That(MatrixOperation.Determinant(matrix), Is.EqualTo(1.0f));
		}

		[Test]
		public void MatrixTransposedTest() {
			double[,] initMatrix = {
				{1, 2},
				{3, 4}
			};

			double[,] correctTransposedMatrix = {
				{1, 3},
				{2, 4}
			};
			Assert.That(MatrixOperation.Transpose(initMatrix), Is.EqualTo(correctTransposedMatrix));
			initMatrix = new double[,]{
				{1, 2, 3},
				{0, 1, 4},
				{5, 6, 0}
			};

			correctTransposedMatrix = new double[,] {
				{1, 0, 5},
				{2, 1, 6},
				{3, 4, 0}
			};
			Assert.That(MatrixOperation.Transpose(initMatrix), Is.EqualTo(correctTransposedMatrix));
		}

		[Test]
		public void MatrixInversedTest() {
			double[,] initMatrix = {
				{1, 2, 3},
				{0, 1, 4},
				{5, 6, 0}
			};
			double[,] correctInversedMatrix = {
				{-24, 18, 5},
				{20, -15, -4},
				{-5, 4, 1}
			};
			Assert.That(MatrixOperation.Inverse3by3Matrix(initMatrix), Is.EqualTo(correctInversedMatrix));
		}

		[Test]
		public void MatrixInverseIncorrectMatrixLength() {
			double[,] matrix = new double[4, 4];
			Assert.Throws(typeof(ArgumentException), () => MatrixOperation.Inverse3by3Matrix(matrix));
			matrix = new double[3, 2];
			Assert.Throws(typeof(ArgumentException), () => MatrixOperation.Inverse3by3Matrix(matrix));
			matrix = new double[2, 3];
			Assert.Throws(typeof(ArgumentException), () => MatrixOperation.Inverse3by3Matrix(matrix));
			matrix = new double[3, 3] {
				{3, -1, 2},
				{5, 1, 0},
				{-2, 3, 4}
			};
			Assert.DoesNotThrow(() => MatrixOperation.Inverse3by3Matrix(matrix));
		}

		[Test]
		[ExpectedException(ExpectedException=typeof(ArgumentException), ExpectedMessage="Matrix is not inversible")]
		public void MatrixInverseNoninversibleMatrix() {
			double[,] matrix = new double[,]{
				{1, 1, 1},
				{1, 1, 1},
				{1, 1, 1}
			};
			MatrixOperation.Inverse3by3Matrix(matrix);
		}

		[Test]
		public void MatrixMinor() {
			double[,] matrix = new double[,]{
				{3, -1, 2},
				{5, 1, 0},
				{-2, 3, 4}
			};
			Assert.That(MatrixOperation.Minor(matrix, 0, 0), Is.EqualTo(new double[,] {
				{1, 0},
				{3, 4}
			}));
			Assert.That(MatrixOperation.Minor(matrix, 1, 1), Is.EqualTo(new double[,] {
				{3, 2},
				{-2, 4}
			}));
			Assert.That(MatrixOperation.Minor(matrix, 2, 1), Is.EqualTo(new double[,] {
				{3, 2},
				{5, 0}
			}));
		}

		[Test]
		public void MatrixCofactors() {
			double[,] matrix = new double[,]{
				{1, 2, 3},
				{0, 4, 5},
				{1, 0, 6}
			};

			double[,] correctCofactors = {
				{24, 5, -4},
				{-12, 3, 2},
				{-2, -5, 4}
			};
			Assert.That(MatrixOperation.Cofactors(matrix), Is.EqualTo(correctCofactors));
		}

		[Test]
		public void GaussJordanReductionSafeCodeTest() {
			decimal[,] matrix = new decimal[,] {
				{1, 1, 1},
				{2, -3, 1},
				{-1, 2, -1}
			};
			decimal[] rhsVector = new decimal[] {4, 2, -1};
			decimal[] correctResult = new decimal[] { 2, 1, 1 };
			Assert.That(MatrixOperation.GaussJordanReduction(matrix, rhsVector, CalculateMethod.SAFE_CODE), Is.EqualTo(correctResult));
		}

		[Test]
		public void GaussJordanReductionUnsafeCodeTest() {
			decimal[,] matrix = new decimal[,] {
				{1, 1, 1},
				{2, -3, 1},
				{-1, 2, -1}
			};
			decimal[] rhsVector = new decimal[] { 4, 2, -1 };
			decimal[] correctResult = new decimal[] { 2, 1, 1 };
			Assert.That(MatrixOperation.GaussJordanReduction(matrix, rhsVector, CalculateMethod.UNSAFE_CODE), Is.EqualTo(correctResult));
		}

		[Ignore]
		[Test]
		public void GaussElimination() {
			decimal[,] matrix = new decimal[,]{
				{1, 2, 3},
				{0, 4, 5},
				{1, 0, 6}
			}; //TODO
			decimal[] rhsVector = new decimal[] { 1, 2, 2 }; //TODO
			decimal[] result = new decimal[] { 1, 1, 1 }; //TODO
			Assert.That(MatrixOperation.GaussElimination(matrix, rhsVector), Is.EqualTo(result));
		}

		[Ignore]
		[Test]
		public void GaussJordanEliminationTest() {

		}

	}
}
