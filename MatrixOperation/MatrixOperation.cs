﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MatrixOperations {
	public enum CalculateMethod { SAFE_CODE, UNSAFE_CODE };

	public class MatrixOperation {
		private static Logger logger = LogManager.GetCurrentClassLogger();

		private static double determinant2by2(double[,] matrix) {
			return (matrix[0, 0] * matrix[1, 1] - (matrix[1, 0] * matrix[0, 1]));
		}

		private static double determinant3by3(double[,] matrix) {
			return (matrix[0, 0] * matrix[1, 1] * matrix[2, 2] +
				matrix[0, 1] * matrix[1, 2] * matrix[2, 0] +
				matrix[0, 2] * matrix[1, 0] * matrix[2, 1] -
				matrix[2, 0] * matrix[1, 1] * matrix[0, 2] -
				matrix[2, 1] * matrix[1, 2] * matrix[0, 0] -
				matrix[2, 2] * matrix[1, 0] * matrix[0, 1]);
		}

		public static double Determinant(double[,] matrix) {
			if (matrix.Length == 4) {
				return determinant2by2(matrix);
			} else if (matrix.Length == 9) {
				return determinant3by3(matrix);
			} else
				throw new ArgumentException("Only 2x2 and 3x3 matrices are allowed");
		}

		public static double[,] Transpose(double[,] matrix) {
			int matrixSize = matrix.GetLength(0);
			double[,] output = new double[matrixSize, matrixSize];
			for (int i = 0; i < matrixSize; i++) {
				for (int j = 0; j < matrixSize; j++) {
					output[i, j] = matrix[j, i];
				}
			}
			return output;
		}

		public static double[,] Cofactors(double[,] matrix) {
			double[,] output = new double[3, 3];
			for (int i = 0; i < matrix.GetLength(0); i++) {
				for (int j = 0; j < matrix.GetLength(1); j++) {
					output[i, j] = (((i + j) % 2 == 0) ? 1 : -1) * Determinant(Minor(matrix, i, j));
				}
			}
			return output;
		}

		public static double[,] Minor(double[,] matrix, int row, int column) {
			double[,] output = new double[2, 2];
			int outputRowCnt = 0, outputColumnCnt = 0;
			for (int i = 0; i < matrix.GetLength(0); i++) {
				if (i == row) continue;
				outputColumnCnt = 0;
				for (int j = 0; j < matrix.GetLength(1); j++) {
					if (j == column) continue;
					output[outputRowCnt, outputColumnCnt] = matrix[i, j];
					outputColumnCnt++;
				}
				outputRowCnt++;
			}
			return output;
		}

		public static double[,] Inverse3by3Matrix(double[,] matrix) {
			//http://www.cs.rochester.edu/~brown/Crypto/assts/projects/adj.html
			// 1/ Determinant, throw if == 0
			// 2/ Transpose
			// 3/ Cofactors - find minors' determinants [minors multiplied by (-1)^(i+j)]
			// 4/ Divide by determinant
			if (matrix.GetLength(0) != 3 || matrix.GetLength(1) != 3)
				throw new ArgumentException("Only 3x3 matrices are supported");
			double[,] output = new double[3, 3];
			double determinant = Determinant(matrix);
			if (determinant == 0) throw new ArgumentException("Matrix is not inversible");
			output = Transpose(matrix);
			output = Cofactors(output);
			for (int i = 0; i < output.Length; i++)
				output[i / 3, i % 3] /= determinant;
			return output;
		}

		/// <summary>
		/// Gauss Elimination
		/// </summary>
		/// <param name="matrix"></param>
		/// <returns>Decimal[] vector of of x-es</returns>
		public static decimal[] GaussElimination(decimal[,] matrix, decimal[] rhsVector) {
			throw new NotImplementedException();
			//matrix x output = rhsVector
			Print(matrix);
			decimal[,] bMatrix = new decimal[matrix.GetLength(0), matrix.GetLength(0) + 1];
			for (int i = 0; i < matrix.GetLength(0); i++) {
				for (int j = 0; j < matrix.GetLength(0); j++) {
					bMatrix[i, j] = matrix[i, j];
				}
				bMatrix[i, bMatrix.GetLength(i)] = rhsVector[i];
				Console.Write(bMatrix[i, bMatrix.GetLength(i)]);
			}
			Print(bMatrix);

			decimal[] output = new decimal[matrix.GetLength(0)];
			for (int i = 1; i < matrix.GetLength(0); i++) {
				for (int j = 0; j < matrix.GetLength(0); j++) {
					matrix[i, j] = matrix[i, j] - (matrix[i, j] / matrix[0, 0]) * matrix[0, j];
				}
			}
		}

		/// <summary>
		/// GaussJordan equation solving
		/// </summary>
		/// <param name="coefficients">square array of coefficients</param>
		/// <param name="rhs">"add-in" array</param>
		/// <returns>results</returns>
		public static decimal[] GaussJordanReduction(decimal[,] coefficients, decimal[] rhs, CalculateMethod calculateMethod = CalculateMethod.SAFE_CODE) {
			switch (calculateMethod) {
				case CalculateMethod.SAFE_CODE:
					return gaussJordanReductionSafeCode(coefficients, rhs);
				case CalculateMethod.UNSAFE_CODE:
					return gaussJordanReductionUnsafeCode(coefficients, rhs);
				default:
					throw new ArgumentException("No calculating method specified.");
			}
		}

		private unsafe static decimal[] gaussJordanReductionUnsafeCode(decimal[,] coefficients, decimal[] rhs) {
			int size = rhs.Length;
			decimal[] output = new decimal[size];

			fixed (decimal* pCoefficients = coefficients, pRhs = rhs, pOutput = output) {
				for (int i = 0; i < size; i++) {
					for (int j = 0; j < i; j++) {
						decimal tmp = pCoefficients[i * size + j] / pCoefficients[j * size + j];
						for (int k = 0; k < size; k++)
							pCoefficients[i * size + k] -= tmp * pCoefficients[j * size + k];
						pRhs[i] -= tmp * pRhs[j];
					}
				}
				for (int i = size - 2; i >= 0; i--) {
					for (int j = size - 1; j > i; j--) {
						decimal tmp = pCoefficients[i * size + j] / pCoefficients[j * size + j];
						for (int k = 0; k < size; k++)
							pCoefficients[i * size + k] -= tmp * pCoefficients[j * size + k];
						pRhs[i] -= tmp * pRhs[j];
					}
				}
				for (int i = 0; i < size; i++) {
					pOutput[i] = pRhs[i] / pCoefficients[i * size + i];
				}
			}
			return output;
		}

		private static decimal[] gaussJordanReductionSafeCode(decimal[,] coefficients, decimal[] rhs) {
			int size = rhs.Length;
			decimal[] output = new decimal[size];
			for (int i = 0; i < size; i++) {
				for (int j = 0; j < i; j++) {
					decimal tmp = coefficients[i, j] / coefficients[j, j];
					for (int k = 0; k < size; k++)
						coefficients[i, k] -= tmp * coefficients[j, k];
					rhs[i] -= tmp * rhs[j];
				}
			}
			for (int i = size - 2; i >= 0; i--) {
				for (int j = size - 1; j > i; j--) {
					decimal tmp = coefficients[i, j] / coefficients[j, j];
					for (int k = 0; k < size; k++)
						coefficients[i, k] -= tmp * coefficients[j, k];
					rhs[i] -= tmp * rhs[j];
				}
			}
			for (int i = 0; i < size; i++) {
				output[i] = rhs[i] / coefficients[i, i];
			}
			return output;
		}

		private static void Print(decimal[,] matrix) {
			for (int i = 0; i < matrix.GetLength(0); i++) {
				for (int j = 0; j < matrix.GetLength(0); j++) {
					//logger.Info("{0} ", matrix[i, j]);
					Console.Write(String.Format("{0} ", matrix[i, j]));
				}
				Console.WriteLine();
				//logger.Info("\n");
			}
		}
	}
}
